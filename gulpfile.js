const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const minify = require('gulp-clean-css');
const path = require('path');

const config = {};
config.project = path.join(__dirname);

config.scss = path.join(config.project, './src/scss');
config.scss_watch = path.join(config.scss, '/**/*.scss');
config.css = path.join(config.project, './src/css');

const build = () => {
    
    return gulp.src(config.scss_watch)
    .pipe(sourcemaps.init())
    .pipe(sass().once('error', sass.logError))
    // .pipe(minify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.css))

}

const watch = () => {
    gulp.watch('./src/scss/**/*.scss', build);
}

exports.build = build;
exports.watch = watch;

