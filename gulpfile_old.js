// Dependencies
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var compass = require('gulp-compass');
var path = require('path');

// config
var config = {};
config.project = path.join(__dirname);

// Sources
config.scss = path.join(config.project, 'scss');
config.scss_root = path.join(config.scss, '/*.scss');
config.scss_watch = path.join(config.scss, '/**/*.scss');

config.css = path.join(config.project, 'css');

config.sourcemap = path.join(config.project, 'souremap');

// sass function
gulp.task('sass', function(){
	gutil.log(config.scss_root);
	gulp.src(config.scss_root)
		.pipe(sourcemaps.init()) // init sourcemaps
		.pipe(plumber({ // error logging with plumber
			showStack: true,
			errorHandler: function (error) {
				gutil.log(error);
				gutil.log(error.message);
				this.emit('end');
			}
		}))
		.pipe(sass({
			style: 'compressed'
		})) // Convert sass to css with gulp sass
		.pipe(compass({
			debug: false,
			sass: config.scss,
			css: config.css,
			sourcemap: true,
		}))
		.pipe(sourcemaps.write(config.sourcemap) // write sourcemap
		.pipe(gulp.dest(config.css)));
});

// listen for changes
gulp.task('watch', function(){
	gulp.watch(config.scss_watch, ['sass']); // call sass function on file change
});

gulp.task("dev", ["scss", "watch"]);

gulp.task("default", ["dev"]);
