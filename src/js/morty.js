jQuery.noConflict();

jQuery(document).ready(function ($) {

  /*----------*\
   # Accordion
  \*----------*/

  $('.js-accordion-header').click(function() {
    accordion($(this).parent().find('.js-accordion-content'));
  });

  function accordion(obj) 
  {
    $(".js-accordion-content").not(obj).slideUp(function() {
      $(this).removeClass("show");
    });

    obj.slideToggle(300, function() {
      $(this).toggleClass("show").css('display', '');
    });
  }

  /*----------*\
   # Hamburger
  \*----------*/ 

  $('.js-menu__toggle').click(function() {
    if($(this).attr('data-menu')) {
      menu = $(this).attr('data-menu');
      toggleChild($(menu));
    }
    hamburger($(this));
  });

  function hamburger(obj) 
  {
    toggleChild(obj.parent().find('.js-menu'));
    obj.find('.menu-bars').toggleClass("menu-cross");
  }

  /*------------*\
   # Menu Child
  \*------------*/ 

  $('.js-menu__parent').click(function(){
    menuchild($(this));
  });

  function menuchild(obj) {
    toggleChild(obj.find('.js-menu__child'));
  }

  function toggleChild(obj) 
  {
    obj.slideToggle(300, function() {
      $(this).toggleClass("show").css('display', '');
    })
  }

  function checkWidth() {
    if ($(window).outerWidth() > 768) {
      $('.morty .c-navbar__collapse').removeClass("show");
      $('.morty .c-menu').removeClass("show");
      $('.js-menu__toggle .menu-bars').removeClass("menu-cross");
    }
  }
  
  $(window).resize(checkWidth);



  /*------------*\
   # Modal
  \*------------*/ 

  $(".js-modal__open").click(function () {
    var target = $(this).attr('data-modal');
    $("#" + target).css("display", "flex");
  });

  $(".js-modal__close").click(function () {
    $(this).closest('.c-modal').css("display", "none");
  });

  $(document).click(function (e) {
    if($(e.target).is(".c-modal")) {
      $(".c-modal").css("display", "none");
    }
  });

  /*------------*\
   # Tabs
  \*------------*/ 

  $('.c-tabs .c-tabs__title').click(function() {
    var target = $(this).attr('data-tab');
    var group = $(this).attr('data-group');
    var selector = ".c-tabs .c-tabs__content" + "[data-group=" + group + "] ";
    console.log(selector);
    $(selector + " .c-tabs__panel").not($(selector + target)).hide();
    $(selector + target).show();
  });

  /*------------*\
   # Offcanvas
  \*------------*/ 

  $('.js-offcanvas__open').click(function() {
    $('.c-offcanvas').addClass('show');
    if($('.c-backdrop')[0]) {
      $('.c-backdrop').show();
    }
  }); 

  $(".js-offcanvas__close").click(function () {
    $(this).closest('.c-offcanvas').removeClass('show');
    if($('.c-backdrop')[0]) {
      $('.c-backdrop').hide();
    }
    console.log("test");
  });

  $(document).click(function (e) {
    if($(e.target).is(".c-backdrop")) {
      $(".c-offcanvas").removeClass('show');
      $('.c-backdrop').hide();
    }
  });

  // Alerts - jQuery
  $(".c-alert .c-alert__close").click(function () {
    $(this).parent().hide();
  })

  // Toggle
  $(".js-panel-header").click(function () {
    console.log("Test");
    toggleChild($(this).parent().find('.js-panel-content'));
  })
});